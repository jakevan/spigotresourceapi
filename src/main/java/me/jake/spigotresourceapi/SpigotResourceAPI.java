/*
MIT License

Copyright (c) 2021 JakeV

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package me.jake.spigotresourceapi;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Jake on 1/5/2021.
 * <insert description here>
 */
public class SpigotResourceAPI {

    private static final String resourceURL = "https://generic-username.gitlab.io/spigotresourceutil/cache.dat";

    public static final HashMap<Integer, ResourceInfo> idData = new HashMap<>();
    public static final HashMap<String, ResourceInfo> nameData = new HashMap<>();
    public static final HashMap<String, ResourceInfo> tagData = new HashMap<>();
    public static final HashMap<String, ResourceInfo> authorData = new HashMap<>();
    public static final AtomicBoolean initialized = new AtomicBoolean(false);

    public static ArrayList<ResourceInfo> searchResources(String match) {
        ArrayList<ResourceInfo> r = new ArrayList<>();
        String[] cases = match.toLowerCase().split(" ");
        for (ResourceInfo value : idData.values()) {
            String lowerName = value.getTitle().toLowerCase();
            boolean good = true;
            for (String ca : cases) {
                if (!lowerName.contains(ca)) {
                    good = false;
                    break;
                }
            }
            if (good) {
                r.add(value);
            }
        }
        return r;
    }

    static void readData() {
        try {
            DataInputStream input = new DataInputStream(getMainDataFile());
            int amount = input.readInt();
            synchronized (SpigotResourceAPI.class) {
                for (int i = 0; i < amount; i++) {
                    ResourceInfo resourceInfo = ResourceInfo.fromData(input);
                    idData.put(resourceInfo.getId(), resourceInfo);
                    nameData.put(resourceInfo.getTitle(), resourceInfo);
                    tagData.put(resourceInfo.getTag(), resourceInfo);
                    authorData.put(resourceInfo.getAuthorUsername(), resourceInfo);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isInitialized() {
        return initialized.get();
    }

    public static void main(String[] args) {
        initialize();
    }

    public static void initialize() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                readData();
                dumpData();
                while (true) {
                    if (initialized.compareAndSet(false, true)) {
                        break;
                    }
                }
            }
        }).start();
    }

    static InputStream getMainDataFile() {
        try {
            HttpURLConnection get = GET(resourceURL);
            int rcode = get.getResponseCode();
            System.err.println("Response code: " + rcode);
            return get.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Could not get data");
    }

    private static HttpURLConnection GET(String urlStr) throws IOException {
        URL url = new URL(urlStr);
        HttpURLConnection openConnection = (HttpURLConnection) url.openConnection();
        openConnection.setRequestMethod("GET");
        openConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
        openConnection.setRequestProperty("User-Agent", "NING/1.0");
        return openConnection;
    }

    static void dumpData() {
        System.out.println("======= BEGIN DUMP =======");
        for (ResourceInfo value : idData.values()) {
            System.out.println(value);
        }

        System.out.println("==========================");
    }
}
